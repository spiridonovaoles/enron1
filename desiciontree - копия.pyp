# -*- coding: utf-8 -*-

import math
import pickle
from bow import *
from matplotlib import pyplot as plt
#from training import *

_minCount = 1
_minEntropy = 0
_maxDepth = 4
_maxEnt = 0.99
_bestClassificatorArr = []
alphas = []
adaResults = []
_M = 4
_validErr = []
_validAcc = []
_validCountClas = []


def CalcEntropy(dataset, labels):
    probab = float(countSpamDataset(labels))/len(dataset)
    #print(probab)
    return -(probab*math.log(probab,2)+(1-probab)*math.log((1-probab),2))

#энтропия для весов
def CalcWeightEntr(weights, labels):
    sumWeight = 0
    for i in xrange(len(weights)):
        sumWeight += weights[i]
    #для спама
    prob = float(CalcWeight(weights, labels))/sumWeight
    if prob == 0.0 or prob == 1.0:
        return 0
    return -(prob*math.log(prob,2)+(1-prob)*math.log((1-prob),2))

#вес спама
def CalcWeight(weights, labels):
    sumWeight = 0
    for i in xrange(len(labels)):
        if labels[i] == 1:
            sumWeight += weights[i]
    return sumWeight
    
def countSpamDataset(labels):
    count = 0
    for i in xrange(len(labels)):
        if labels[i] == 1 :
            count = count+1
    return count
            
class Node:
    def __init__(self, depth):
        #self.left = Node()
        #self.right = Node()
        self.depth = depth
        self.isTerm = False
        
    def divide(self, dataset, labels, weights): 
        if(len(dataset) != 0):
            #self.probab = float(countSpamDataset(labels))/len(dataset) #вероятность спама         
            self.probab = float(CalcWeight(weights, labels))/len(dataset) #вероятность спама
            #self.curEntropy = CalcEntropy(dataset, labels) 
            self.curEntropy = CalcWeightEntr(weights, labels) 
        if (len(dataset) <= _minCount or self.curEntropy <= _minEntropy or self.depth == _maxDepth): 
            self.isTerm = True
            return 
        self.bestVal, self.index = self.SplitDataset(dataset, labels, weights)
        #print(self.bestVal)
        #print(self.index)
        lDS = [] 
        rDS = []
        lLb = []
        rLb = []
        lWg = []
        rWg = []
        for i in xrange(len(dataset)): 
            if dataset[i][self.index]<self.bestVal: 
                lDS.append(dataset[i])
                lLb.append(labels[i])
                lWg.append(weights[i])
            else: 
                rDS.append(dataset[i])
                rLb.append(labels[i])
                rWg.append(weights[i])
        #создаем потомков и увеличиваем глубину дерева 
        self.left = Node(self.depth+1) 
        self.left.divide(lDS, lLb, lWg) 
        self.right = Node(self.depth+1) 
        self.right.divide(rDS, rLb, rWg)
        
    def classify(self, vector):
        if(self.isTerm):
            if self.probab > 0.5:
                return 1
            else:
                return -1
            #return self.probab
        else:
            if(vector[self.index] < self.bestVal):
                return self.left.classify(vector)
            else:
                return self.right.classify(vector)
             
    def SplitDataset(self, dataset, labels, weigths):
        entropies = [] #для лучших энтропий по каждой характеристике
        bestTetas = [] #соответствующие теты
        indexOfCharacter = -1 #индекс характеристики для разбиения
        print(len(dataset[0]))
        for j in xrange(len(dataset[0])): #по всем характеристикам
            mini = dataset[0][j]
            maxi = dataset[0][j]            
            for i in xrange(len(dataset)):
                if mini > dataset[i][j]:
                    mini = dataset[i][j]
                if maxi < dataset[i][j]:
                    maxi = dataset[i][j]
            tetas = np.linspace(mini, maxi, 50) #формируем теты
            #print(tetas)
            minEnt = 1
            index = -1
            #ищем лучшую тету и ее энтропию
            for i in xrange(len(tetas)):
               leftDS = []
               leftWeightDS = []
               lLab = []
               rightDS = []
               rightWeightDS = []
               rLab = []
               for k in xrange(len(dataset)):
                  # print(tetas[i])
                  # print(j)
                   if dataset[k][j] < tetas[i]:
                       leftDS.append(dataset[k][j])
                       leftWeightDS.append(weigths[k])
                       lLab.append(labels[k])
                   else:
                       rightDS.append(dataset[k][j])
                       rightWeightDS.append(weigths[k])
                       rLab.append(labels[k])
               leftEnt=1
               rightEnt=1
               if len(leftDS) != 0:
                   #leftEnt = CalcEntropy(leftDS, lLab)
                   leftEnt = CalcWeightEntr(leftWeightDS, lLab)
               if len(rightDS) != 0:    
                   #rightEnt = CalcEntropy(rightDS, rLab)
                   rightEnt = CalcWeightEntr(rightWeightDS, rLab)
               #подсчет энтропии суммарной
               entSum = (leftEnt*np.double(np.sum(leftWeightDS)) + rightEnt*np.double(np.sum(rightWeightDS)))/(np.sum(leftWeightDS)+np.sum(rightWeightDS))
               if i == 0:
                   minEnt = entSum
                   index = i
               if(entSum < minEnt): #поиск минимальной энтропии и соответствующего ей № теты для текущей характеристики
                   minEnt = entSum
                   index = i
            entropies.append(minEnt) #добавляем энтропию
            bestTetas.append(tetas[index]) #добавляем тету
        #ищем лучшую энтропию и возвращаем характеристику       
        bestEntr = 1
        bestTeta = -1
        for i in xrange(len(entropies)):
            if i == 0:
               bestEntr = entropies[i]
               bestTeta = bestTetas[i] 
               indexOfCharacter = i
            if(bestEntr>entropies[i]):
               bestEntr = entropies[i]
               bestTeta = bestTetas[i] 
               indexOfCharacter = i
        return bestTeta, indexOfCharacter    
    
def loadTree(filepath):
    input_file = open(filepath)
    obj = pickle.load(input_file)
    input_file.close()
    return obj

def dumpTree(obj, filepath):
    output_file = open(filepath, "wb")
    pickle.dump(obj, output_file)
    output_file.close()

def trainTree(weights, labels, dataset, i):
    node = Node(1)
    node.divide(dataset, labels, weights)
    dumpTree(node, 'tree_' + str(i) +'.pickle')
    return node
    
def ourSign(classifyVal, labelVal):
    if(classifyVal != labelVal):
        return 1
    else:
        return -1
    
def adaTrain(dataset,labels): #labels
    N = len(dataset)
    w = np.ones(N)/N
    #for i in xrange(N):
        #w[i] = 1.0/N
    #print 'N', N
    #print 'w', w
    for j in xrange(_M):
        node = trainTree(w, labels, dataset, j+1)
        e = np.double(0)
        for i in xrange(len(dataset)):
            '''print 'w', w[i]
            print 'node', node.classify(dataset[i])
            print 'labels', labels[i]'''
            if node.classify(dataset[i]) != labels[i]:
                e += w[i]
        
        err = e
        #print w
        print 'e', err
        if (err == np.double(1)): #защита от деления на нолик
            print 'error = 1', err
            err -= 1e-16
        if str(e) == str(0.5):   
            print 'error = 0.5'
            return
        _bestClassificatorArr.append(node)
        a = math.log(float(1-err)/float(err))
        alphas.append(a)
        
        if np.double(err) == np.double(0):
            return
       
        z = 0.0
        for i in xrange(len(dataset)):
            if node.classify(dataset[i]) != labels[i]:
                z += w[i] * np.exp(a)
            else:
                z+=w[i]
        for i in xrange(len(dataset)):
            if node.classify(dataset[i]) != labels[i]:
                w[i] = (w[i] * np.exp(a))/z
            else:
                w[i] = w[i]/z
                

'''
def adaTrainFiles(dataset, labels): #labels
    N = len(dataset)
    w = np.ones(N)/N
    print 'w', w
    for j in xrange(_M):
	node = trainTree(w, labels, dataset, j)
	e = 0
	for i in xrange(len(dataset)):
	    if node.classify(dataset[i]) != labels[i]:
            	e += w[i]
      if e == 0.5:
	     return
        #_bestClassificatorArr.append(node)
        a = math.log((1-e)/e)
        alphas.append(a)
	z = 0
	for i in xrange(len(dataset)):
	    if node.classify(dataset[i]) != labels[i]:
            		z += w[i] * np.exp(a)
    	for i in xrange(len(dataset)):
	    if node.classify(dataset[i]) != labels[i]:
            		w[i] = w[i] * np.exp(a)/z
'''
def adaClassify(dataset, countCl):
    for i in xrange(len(dataset)):
        res = 0
        k = 0
        
        while k < countCl:
            #print 'k', k
           #print 'alph', alphas[k]
           # print 'i', i
            res += float(alphas[k]) * float(_bestClassificatorArr[k].classify(dataset[i]))
            k += 1  
            #print 'k', k
            
        adaResults.append(np.sign(res))
        #print 'adaResults', adaResults
        #print 'print alphas', alphas
'''
def adaClassifyFiles(dataset):
    for i in xrange(len(dataset)):
        res = 0
        for j in xrange(len(alphas)):
            node = loadTree('tree_' + str(j) +'.pickle')
            res += alphas[j] * node.classify(dataset[i])
            if np.sign(res) == 0:            
                print 'RES', res
                print 'ReSi', np.sign(res)
        adaResults.append(np.sign(res))
'''
def testing():
    print 'len(testingSet)', len(testingSet) 
    print 'len(validSet)', len(validSet)
    print 'len(trainingSet)', len(trainingSet)
    adaTrain(trainingSet, trainingLbl)
    #dumpTree(_bestClassificatorArr, 'adaboost.pickle')
    #dumpTree(alphas, 'alphas.pickle')
    #_bestClassificatorArr = loadTree('adaboost.pickle')
    #alphas = loadTree('alphas.pickle')
    adaClassify(trainingSet, len(alphas))
    adaClassify(testingSet, len(alphas))
    adaClassify(validSet, len(alphas))

    #adaTrainFiles(trainigSet, trainingLbl)
    #adaClassifyFiles(testingSet, testingLbl)
    accur = accuracy(testingSet, testingLbl)
    accurTrain = accuracy(trainingSet, trainingLbl)
    accurValid = accuracy(validSet, validLbl)
    #print('TruePositive on test: ' + str(acc))
    #print('Error on test: ' + str(er))
    errTrain = (float(len(trainingSet) - accurTrain))/float(len(trainingSet))
    erTest = (float(len(testingSet) - accur))/float(len(testingSet))
    erValid = (float(len(validSet) - accurValid))/float(len(validSet))
    print 'errTrain', errTrain
    print 'erTest', erTest
    print 'erValid', erValid
    return errTrain, erTest

def accuracy(dataset, labels):
    accur = 0
    #print 'adaResults', adaResults
    for i in xrange(len(dataset)):
        if (adaResults[i] == 1 and labels[i]==1):
            accur +=1
        elif (adaResults[i] != 1 and labels[i]!=1):
            accur +=1
    return accur

def validation():
    R = len(alphas) # КОЛИЧЕСТВО КЛАССИФИКАТОРОВ
    for j in xrange(R):
        #R -= 1
        adaClassify(validSet, j+1)
        tempAcc = accuracy(validSet, validLbl)
        print 'tempAcc', tempAcc
        _validAcc.append((float(tempAcc))/(float(len(validSet))))
        _validErr.append((float(len(validSet) - tempAcc))/float(len(validSet)))
        _validCountClas.append(j+1)
    #print 'accuracy(validSet, validLbl)', accuracy(validSet, validLbl)
    #print 'len(validSet)', len(validSet)
    #print 'float', accuracy(validSet, validLbl)/len(validSet)
    print _validCountClas
    print _validAcc
    print _validErr
    plt.plot(_validCountClas,_validAcc)
    plt.savefig("accuracy.png")
    plt.show()
    plt.plot(_validCountClas,_validErr)
    plt.savefig("error.png")
    #plt.show()
	
Parser("C:\Users\Asus\Desktop\enron1") 
#adaTrain(trainingSet, trainingLbl)  
testing()
validation()
#x = np.array([[1,0,3],[1,1,3],[1,0,3],[0,0,1],[1,2,3],[1,3,0]])
#y = np.array([1,-1,1,1,-1,1])
#w = np.array([np.double(1)/6,np.double(1)/6,np.double(1)/6,np.double(1)/6,np.double(1)/6,np.double(1)/6])
#adaBoost(ans, marks, 10)  
        
