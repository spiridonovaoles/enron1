# -*- coding: utf-8 -*-

import os
import codecs
import pickle
import numpy as np

dic = {}
marks = []
ans = []
validSet = []
testingSet = []
trainingSet = []
validLbl = []
testingLbl = []
trainingLbl = []   

def dumpTree(obj, filepath):
    output_file = open(filepath, "wb")
    pickle.dump(obj, output_file)
    output_file.close()
    
 #это лист словарей
def Parser(path, json_name='ans.json'):
    ham_path = path + '/ham' #путь к папке
    hams = os.listdir(ham_path) #получить файлы в папке
    spam_path = path + '/spam' #путь к папке
    spams = os.listdir(spam_path) #получить файлы в папке

    for ham in hams: #для не спама
        get_dictionary(dic, ham_path+'/'+ham)
        marks.append(-1) # ham
    for spam in spams: #то же, но для спама
        get_dictionary(dic, spam_path+'/'+spam)
        marks.append(1) # spam
    for key, value in dic.items():
        if value < 10:
            del dic[key]
    #бежим по всем письмам и собираем все слова
    #print 'total dictionary', dic
    count = 0 
    for ham in hams: #для не спама
        newDic = pars_current_letter(dic, ham_path+'/'+ham)
        newItem = []                
        for i in xrange(len(newDic)):
            newItem.append(newDic.values()[i])

        if count < 20: #50
            testingSet.append(newItem)
            testingLbl.append(-1)
        elif count < 30: #90
            validSet.append(newItem)
            validLbl.append(-1)
        else:
            trainingSet.append(newItem)
            trainingLbl.append(-1)
        count += 1
        ans.append(newItem)
	    #print(ans[int(name)-1])
    #print 'falseValid',validLbl
    count = 0
    for spam in spams: #то же, но для спама]
        newDic = pars_current_letter(dic, ham_path+'/'+ham)
        newItem = []                
        for i in xrange(len(newDic)):
            newItem.append(newDic.values()[i])
        if count < 50:
            testingSet.append(newItem)
            testingLbl.append(1)
        elif count < 100:
            validSet.append(newItem)
            validLbl.append(1)
        else:
            trainingSet.append(newItem)
            trainingLbl.append(1)
        count += 1
        ans.append(newItem)
        #ans.append(pars_current_letter(dic, spam_path+'/'+spam))
    #print 'trueValid',validLbl
    dict1=np.zeros(len(testingSet[0]))
    dumpTree(dict1, 'dictionary.pickle')
    print len(testingSet) 
    print len(validSet)
    print len(trainingSet)
    #print ans
    #print marks #ТУТ оценки
    #return ans



def get_dictionary(dic, letter_path): #построение словаря
    file = codecs.open(letter_path, 'r', encoding='utf-8', errors='ignore') #прочитать письмо
    letter = file.read()
    file.close()

    letter = letter.replace('!', '') #удалить мешающие символы
    letter = letter.replace('?', '')
    letter = letter.replace('.', '')
    letter = letter.replace(',', '')
    letter = letter.replace(':', '')
    letter = letter.replace(' a ', '')
    letter = letter.replace(' the ', '')
    letter = letter.replace(' an ', '')
    letter = letter.replace(' at ', '')
    letter = letter.replace(' am ', '')
    letter = letter.replace(' m ', '')
    letter = letter.replace(' are ', '')
    letter = letter.replace(' re ', '')
    letter = letter.replace(' p ', '')
    letter = letter.replace(' for ', '')
    letter = letter.replace(' to ', '')
    letter = letter.replace(' is ', '')
    letter = letter.replace(' d ', '')
    letter = letter.replace(' in ', '')
    letter = letter.replace(' and ', '')
    letter = letter.replace(' that ', '')
    letter = letter.replace(' this ', '')
    letter = letter.replace(' of ', '')
    letter = letter.replace(' s ', '')
    
    for string in letter.split('\n'): #идём по строкам письма
        for word in string.split(' '): #идём по словам строки
            if word.isalpha(): #если это слово (не цифра и не знак)
                if word not in dic: #если такого ещё небыло
                    dic[word] = 1
                else:
                    dic[word] +=1





def pars_current_letter(dic, letter_path): #построение словаря для одного письма
    file = codecs.open(letter_path, 'r', encoding='utf-8', errors='ignore') #прочитать письмо
    letter = file.read()
    file.close()
    current_dic = {}
    for i in dic:
	dic[i] = 0
    for i in current_dic:
	current_dic[i] = 0

    current_dic = dic
    for string in letter.split('\n'): #идём по строкам письма
        for word in string.split(' '): #идём по словам строки
            if word.isalpha(): #если это слово (не цифра и не знак)
                if word in dic:
                    current_dic[word] += 1

    return dict(current_dic)


